<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class QuranController extends Controller
{
    public function index(){
        $response = Http::get('https://equran.id/api/v2/surat');
        $data = $response->json();

        return view('alquran',['data' => $data]);
    }

    public function detail($id){
        $response = Http::get('https://equran.id/api/v2/surat/' . $id);
        $data = $response->json();
        $ayat = $data['data']['ayat'];

        return view('detail', ['data' => $data, 'ayat' => $ayat]);

        // foreach ($ayat['data'] as $result)
        // {
        //     $namaLatin = $result['namaLatin'];

        //     $detail[] = array(

        //         'namaLatin' => $namaLatin,

        //     );            
        // }
        // return $detail;
        // echo '<pre>';
        // print_r($ayat);
        // echo '</pre>';
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';



    }


}
