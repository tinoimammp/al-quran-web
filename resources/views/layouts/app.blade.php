<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    {{-- SEO Tags --}}
    @include('e-arsip.includes.meta')
  <title>{{ $title }} | Kubikal E-Arsip</title>
  {{-- Default CSS --}}
    @include('e-arsip.includes.style')
</head>

<body onload="startTime()">
  <!-- loader starts-->
  <div class="loader-wrapper">
    <div class="loader-index"> <span></span></div>
    <svg>
      <defs />
      <filter id="goo">
        <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur" />
        <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo"> </fecolormatrix>
      </filter>
    </svg>
  </div>
  <!-- loader ends-->
  <!-- page-wrapper Start-->
  <div class="page-wrapper compact-wrapper" id="pageWrapper">
    {{-- Page Header Start --}}
    @include('e-arsip.includes.header')
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
    {{-- Page sidebar Start  --}}
    @include('e-arsip.includes.sidebar')
    {{-- Page Content Start --}}
      @yield('content')

      <!-- footer start-->
    @include('e-arsip.includes.footer')

    
    </div>
  </div>
  {{-- Default Script --}}
    @include('e-arsip.includes.script')
  {{-- Tambahan Javascript Lain --}}
    @stack('scripts')
</body>

</html>