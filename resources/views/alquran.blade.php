<!DOCTYPE html><!-- This site was created in Webflow. https://www.webflow.com --><!-- Last Published: Thu Mar 23 2023 20:40:09 GMT+0000 (Coordinated Universal Time) -->
<html data-wf-domain="localhost.co" data-wf-page="63bf4688ee0ab1ed1d54512f" data-wf-site="63bed0273cfe5e3f80742329" data-wf-status="1" lang="en">
   <head>
      <meta charset="utf-8"/>
      <title>Companies - Aggregator X - Webflow Ecommerce website template</title>
      <meta content="Are you looking to build your own curation site like ProductHunt, HackerNews or DesignerNews? Aggregator X is the ultimate curated directory Webflow Template for that!" name="description"/>
      <meta content="Companies - Aggregator X - Webflow Ecommerce website template" property="og:title"/>
      <meta content="Are you looking to build your own curation site like ProductHunt, HackerNews or DesignerNews? Aggregator X is the ultimate curated directory Webflow Template for that!" property="og:description"/>
      <meta content="https://assets.website-files.com/63bed0273cfe5e3f80742329/63bf4754cfc6058740bff0f1_meta-aggregator-x-webflow-template.png" property="og:image"/>
      <meta content="Companies - Aggregator X - Webflow Ecommerce website template" property="twitter:title"/>
      <meta content="Are you looking to build your own curation site like ProductHunt, HackerNews or DesignerNews? Aggregator X is the ultimate curated directory Webflow Template for that!" property="twitter:description"/>
      <meta content="https://assets.website-files.com/63bed0273cfe5e3f80742329/63bf4754cfc6058740bff0f1_meta-aggregator-x-webflow-template.png" property="twitter:image"/>
      <meta property="og:type" content="website"/>
      <meta content="summary_large_image" name="twitter:card"/>
      <meta content="width=device-width, initial-scale=1" name="viewport"/>
      <meta content="Webflow" name="generator"/>
      <link href="https://assets.website-files.com/63bed0273cfe5e3f80742329/css/aggregatortemplate.webflow.f4c058c4c.css" rel="stylesheet" type="text/css"/>
      <link href="https://fonts.googleapis.com" rel="preconnect"/>
      <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin="anonymous"/>
      <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script><script type="text/javascript">WebFont.load({  google: {    families: ["DM Sans:regular,500,700"]  }});</script><!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif]--><script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
      <link href="https://assets.website-files.com/63bed0273cfe5e3f80742329/63bedef28655c63f924b43a7_favicon-aggregator-x-webflow-template.svg" rel="shortcut icon" type="image/x-icon"/>
      <link href="https://assets.website-files.com/63bed0273cfe5e3f80742329/63bedeff7ed22e5d022b5294_webclip-aggregator-x-webflow-template.svg" rel="apple-touch-icon"/>
   </head>
   <body>
      <div style="opacity:0" class="page-wrapper">
         <div data-w-id="3e4eae5f-c9fd-c051-dc9f-9317b4043bae" data-animation="default" data-collapse="medium" data-duration="400" data-easing="ease" data-easing2="ease" role="banner" class="header-wrapper w-nav">
            <div class="container-default w-container">
               <div class="header-content-wrapper">
                  <div class="header-left-side flex-growth-tablet">
                     <a href="/home" class="header-logo-link left w-nav-brand"><img src="https://assets.website-files.com/63bed0273cfe5e3f80742329/63bee93eef68b418789384d2_black-logo-aggregator-x-webflow-template.svg" alt="Aggregator X Webflow Template - Logo"/></a>
                     <nav role="navigation" class="header-nav-menu-wrapper w-nav-menu">
                        <ul role="list" class="header-nav-menu-list">
                           <li class="header-nav-list-item left"><a href="/home" class="header-nav-link w-nav-link">Home</a></li>
                           <li class="header-nav-list-item left"><a href="/products" class="header-nav-link w-nav-link">Products</a></li>
                           <li class="header-nav-list-item left">
                              <div data-hover="true" data-delay="0" data-w-id="e82a16f0-9b01-13bc-d231-3f58b93d6857" class="dropdown-wrapper w-dropdown">
                                 <div class="dropdown-toggle w-dropdown-toggle">
                                    <div>Pages</div>
                                    <div class="line-rounded-icon dropdown-arrow"></div>
                                 </div>
                                 <nav class="dropdown-column-wrapper w-dropdown-list">
                                    <div class="dropdown-pd">
                                       <div class="w-layout-grid grid-3-columns dropdown-subgrid-main">
                                          <div id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6860-b4043bae">
                                             <div class="dropdown-title">Main pages</div>
                                             <div class="w-layout-grid grid-3-columns dropdown-subgrid">
                                                <div id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6864-b4043bae" class="w-layout-grid grid-1-column dropdown-link-column"><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6865-b4043bae" href="/home" class="dropdown-link">Home</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6867-b4043bae" href="/" class="dropdown-link">Home (Sales)</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6869-b4043bae" href="/about" class="dropdown-link">About</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d686b-b4043bae" href="/products" class="dropdown-link">Products</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d686d-b4043bae" href="https://aggregatortemplate.webflow.io/product-categories/business" class="dropdown-link">Product category</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d686f-b4043bae" href="https://aggregatortemplate.webflow.io/products/startuper" class="dropdown-link">Product single</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6871-b4043bae" href="https://aggregatortemplate.webflow.io/product-tags/communication" class="dropdown-link">Product tag</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6873-b4043bae" href="/creators" class="dropdown-link">Creators</a></div>
                                                <div id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6875-b4043bae" class="w-layout-grid grid-1-column dropdown-link-column"><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6876-b4043bae" href="https://aggregatortemplate.webflow.io/creator/john-carter" class="dropdown-link">Creators single</a><a id="w-node-a8cbe737-6db5-cf6b-5301-ebff30391526-b4043bae" href="/moderators" class="dropdown-link">Moderators</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6878-b4043bae" href="https://aggregatortemplate.webflow.io/moderators/lilly-woods" class="dropdown-link">Moderator single</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d687a-b4043bae" href="/companies" aria-current="page" class="dropdown-link w--current">Companies</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d687c-b4043bae" href="https://aggregatortemplate.webflow.io/companies/webflow" class="dropdown-link">Company single</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d687e-b4043bae" href="/post-a-product" class="dropdown-link">Post a product</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6880-b4043bae" href="/blog" class="dropdown-link">Blog</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6882-b4043bae" href="https://aggregatortemplate.webflow.io/blog-categories/marketing" class="dropdown-link">Blog category</a></div>
                                                <div id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6884-b4043bae" class="w-layout-grid grid-1-column dropdown-link-column"><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6885-b4043bae" href="https://aggregatortemplate.webflow.io/blog/10-product-design-tips-to-make-users-fall-in-love-with-your-app" class="dropdown-link">Blog post</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6887-b4043bae" href="/contact" class="dropdown-link">Contact</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6889-b4043bae" href="/credits" class="dropdown-link">Credits</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d688b-b4043bae" href="https://aggregatortemplate.webflow.io/product/4-weeks-featured" class="dropdown-link">Credit single</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d688d-b4043bae" href="/coming-soon" class="dropdown-link">Coming soon</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d688f-b4043bae" href="https://brixtemplates.com/more-webflow-templates" target="_blank" class="more-webflow-templates-link">More Webflow Templates</a></div>
                                             </div>
                                          </div>
                                          <div id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d6891-b4043bae">
                                             <div class="dropdown-title">Categories</div>
                                             <div class="categories-card-wrapper v1">
                                                <div class="w-dyn-list">
                                                   <div role="list" class="grid-1-column categories-grid w-dyn-items">
                                                      <div role="listitem" class="w-dyn-item">
                                                         <a data-w-id="72e2c668-2b87-d42c-82a0-3dec1321ed42" href="/product-categories/business" class="category---link-wrapper w-inline-block">
                                                            <div class="category---description-wrapper">
                                                               <div><img src="https://assets.website-files.com/63bed0273cfe5e611e742359/63c57f0a578243231624ebd4_briefcase-aggregator-x-webflow-template.svg" alt="Business" class="_w-h-20px"/></div>
                                                               <div class="category---description">
                                                                  <div class="category---title">Business</div>
                                                                  <div class="category---short-description">Massa sit praesent leo morbi</div>
                                                               </div>
                                                            </div>
                                                            <div class="categories---arrow-right">
                                                               <div class="line-rounded-icon link-icon-right"></div>
                                                            </div>
                                                         </a>
                                                      </div>
                                                      <div role="listitem" class="w-dyn-item">
                                                         <a data-w-id="72e2c668-2b87-d42c-82a0-3dec1321ed42" href="/product-categories/design" class="category---link-wrapper w-inline-block">
                                                            <div class="category---description-wrapper">
                                                               <div><img src="https://assets.website-files.com/63bed0273cfe5e611e742359/63c57eeec8b8535877b91cd0_pencil-aggregator-x-webflow-template.svg" alt="Design" class="_w-h-20px"/></div>
                                                               <div class="category---description">
                                                                  <div class="category---title">Design</div>
                                                                  <div class="category---short-description">Donec aliquet urna eu</div>
                                                               </div>
                                                            </div>
                                                            <div class="categories---arrow-right">
                                                               <div class="line-rounded-icon link-icon-right"></div>
                                                            </div>
                                                         </a>
                                                      </div>
                                                      <div role="listitem" class="w-dyn-item">
                                                         <a data-w-id="72e2c668-2b87-d42c-82a0-3dec1321ed42" href="/product-categories/development" class="category---link-wrapper w-inline-block">
                                                            <div class="category---description-wrapper">
                                                               <div><img src="https://assets.website-files.com/63bed0273cfe5e611e742359/63c57ec7f0ee7312b9f4e65a_monitor-aggregator-x-webflow-template.svg" alt="Development" class="_w-h-20px"/></div>
                                                               <div class="category---description">
                                                                  <div class="category---title">Development</div>
                                                                  <div class="category---short-description">Suspendisse et non et dolor</div>
                                                               </div>
                                                            </div>
                                                            <div class="categories---arrow-right">
                                                               <div class="line-rounded-icon link-icon-right"></div>
                                                            </div>
                                                         </a>
                                                      </div>
                                                      <div role="listitem" class="w-dyn-item">
                                                         <a data-w-id="72e2c668-2b87-d42c-82a0-3dec1321ed42" href="/product-categories/marketing" class="category---link-wrapper w-inline-block">
                                                            <div class="category---description-wrapper">
                                                               <div><img src="https://assets.website-files.com/63bed0273cfe5e611e742359/63c57e995c1979ddb6479977_pencil-simple-aggregator-x-webflow-template.svg" alt="Marketing" class="_w-h-20px"/></div>
                                                               <div class="category---description">
                                                                  <div class="category---title">Marketing</div>
                                                                  <div class="category---short-description">Id morbi eget velit</div>
                                                               </div>
                                                            </div>
                                                            <div class="categories---arrow-right">
                                                               <div class="line-rounded-icon link-icon-right"></div>
                                                            </div>
                                                         </a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d68c6-b4043bae">
                                             <div class="dropdown-title">Utility Pages</div>
                                             <div class="w-layout-grid grid-1-column dropdown-link-column"><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d68ca-b4043bae" href="/template-pages/start-here" class="dropdown-link">Start here</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d68cc-b4043bae" href="/template-pages/style-guide" class="dropdown-link">Styleguide</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d68ce-b4043bae" href="https://aggregatortemplate.webflow.io/404" class="dropdown-link">404 Not found</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d68d0-b4043bae" href="https://aggregatortemplate.webflow.io/401" class="dropdown-link">Password protected</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d68d2-b4043bae" href="/template-pages/licenses" class="dropdown-link">Licenses</a><a id="w-node-e82a16f0-9b01-13bc-d231-3f58b93d68d4-b4043bae" href="/template-pages/changelog" class="dropdown-link">Changelog</a></div>
                                          </div>
                                       </div>
                                    </div>
                                 </nav>
                              </div>
                           </li>
                           <li class="header-nav-list-item left"><a href="/contact" class="header-nav-link w-nav-link">Contact</a></li>
                           <li class="header-nav-list-item show-in-tablet"><a href="/post-a-product" class="btn-secondary small w-button">Post a free product</a></li>
                           <li class="header-nav-list-item show-in-tablet"><a href="/post-a-product" class="btn-primary small w-button"><span class="filled-icon-star"> </span>Post a featured product</a></li>
                        </ul>
                     </nav>
                     <div data-node-type="commerce-cart-wrapper" data-open-product="" data-wf-cart-type="modal"  data-wf-page-link-href-prefix="" class="w-commerce-commercecartwrapper cart-button-wrapper mg-left-auto-tablet">
                        <a href="#" data-node-type="commerce-cart-open-link" data-w-id="e82a16f0-9b01-13bc-d231-3f58b93d68e2" class="w-commerce-commercecartopenlink cart-button v1 w-inline-block">
                           <div>Cart (</div>
                           <div data-wf-bindings="%5B%7B%22innerHTML%22%3A%7B%22type%22%3A%22Number%22%2C%22filter%22%3A%7B%22type%22%3A%22numberPrecision%22%2C%22params%22%3A%5B%220%22%2C%22numberPrecision%22%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItemsCount%22%7D%7D%5D" class="w-commerce-commercecartopenlinkcount cart-quantity v1">0</div>
                           <div>)</div>
                        </a>
                        <div data-node-type="commerce-cart-container-wrapper" style="display:none" class="w-commerce-commercecartcontainerwrapper w-commerce-commercecartcontainerwrapper--cartType-modal cart-wrapper">
                           <div data-node-type="commerce-cart-container" class="w-commerce-commercecartcontainer cart-container">
                              <div class="card cart">
                                 <div class="w-commerce-commercecartheader cart-header">
                                    <h4 class="w-commerce-commercecartheading heading-h4-size">Your Cart</h4>
                                    <a href="#" data-node-type="commerce-cart-close-link" class="w-commerce-commercecartcloselink cart-close-button w-inline-block">
                                       <div class="line-square-icon"></div>
                                    </a>
                                 </div>
                                 <div class="w-commerce-commercecartformwrapper cart-form-wrapper">
                                    <form data-node-type="commerce-cart-form" style="display:none" class="w-commerce-commercecartform">
                                       <script type="text/x-wf-template" id="wf-template-e82a16f0-9b01-13bc-d231-3f58b93d68f4">%3Cdiv%20class%3D%22w-commerce-commercecartitem%20cart-item-wrapper%22%3E%3Cdiv%20class%3D%22cart-flex%22%3E%3Cimg%20data-wf-bindings%3D%22%255B%257B%2522src%2522%253A%257B%2522type%2522%253A%2522ImageRef%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.sku.f_main_image_4dr%2522%257D%257D%255D%22%20src%3D%22%22%20alt%3D%22%22%20class%3D%22w-commerce-commercecartitemimage%20cart-image-image%20border-radius-8px%20w-dyn-bind-empty%22%2F%3E%3Cdiv%20class%3D%22w-commerce-commercecartiteminfo%20mg-left-right-0-mbp%22%3E%3Cdiv%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.product.f_name_%2522%257D%257D%255D%22%20class%3D%22w-commerce-commercecartproductname%20cart-item-title%20w-dyn-bind-empty%22%3E%3C%2Fdiv%3E%3Cdiv%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522CommercePrice%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522price%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.price%2522%257D%257D%255D%22%20class%3D%22cart-item-price%22%3E%24%C2%A00.00%C2%A0USD%3C%2Fdiv%3E%3Cscript%20type%3D%22text%2Fx-wf-template%22%20id%3D%22wf-template-e82a16f0-9b01-13bc-d231-3f58b93d68fb%22%3E%253Cli%2520class%253D%2522mg-bottom-4px%2520pd-left-0%2522%253E%253Cspan%2520data-wf-bindings%253D%2522%25255B%25257B%252522innerHTML%252522%25253A%25257B%252522type%252522%25253A%252522PlainText%252522%25252C%252522filter%252522%25253A%25257B%252522type%252522%25253A%252522identity%252522%25252C%252522params%252522%25253A%25255B%25255D%25257D%25252C%252522dataPath%252522%25253A%252522database.commerceOrder.userItems%25255B%25255D.product.f_sku_properties_3dr%25255B%25255D.name%252522%25257D%25257D%25255D%2522%2520class%253D%2522w-dyn-bind-empty%2522%253E%253C%252Fspan%253E%253Cspan%253E%253A%2520%253C%252Fspan%253E%253Cspan%2520data-wf-bindings%253D%2522%25255B%25257B%252522innerHTML%252522%25253A%25257B%252522type%252522%25253A%252522CommercePropValues%252522%25252C%252522filter%252522%25253A%25257B%252522type%252522%25253A%252522identity%252522%25252C%252522params%252522%25253A%25255B%25255D%25257D%25252C%252522dataPath%252522%25253A%252522database.commerceOrder.userItems%25255B%25255D.product.f_sku_properties_3dr%25255B%25255D%252522%25257D%25257D%25255D%2522%2520class%253D%2522w-dyn-bind-empty%2522%253E%253C%252Fspan%253E%253C%252Fli%253E%3C%2Fscript%3E%3Cul%20data-wf-bindings%3D%22%255B%257B%2522optionSets%2522%253A%257B%2522type%2522%253A%2522CommercePropTable%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.product.f_sku_properties_3dr%5B%5D%2522%257D%257D%252C%257B%2522optionValues%2522%253A%257B%2522type%2522%253A%2522CommercePropValues%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.sku.f_sku_values_3dr%2522%257D%257D%255D%22%20class%3D%22w-commerce-commercecartoptionlist%22%20data-wf-collection%3D%22database.commerceOrder.userItems%255B%255D.product.f_sku_properties_3dr%22%20data-wf-template-id%3D%22wf-template-e82a16f0-9b01-13bc-d231-3f58b93d68fb%22%3E%3Cli%20class%3D%22mg-bottom-4px%20pd-left-0%22%3E%3Cspan%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.product.f_sku_properties_3dr%255B%255D.name%2522%257D%257D%255D%22%20class%3D%22w-dyn-bind-empty%22%3E%3C%2Fspan%3E%3Cspan%3E%3A%20%3C%2Fspan%3E%3Cspan%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522CommercePropValues%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.product.f_sku_properties_3dr%255B%255D%2522%257D%257D%255D%22%20class%3D%22w-dyn-bind-empty%22%3E%3C%2Fspan%3E%3C%2Fli%3E%3C%2Ful%3E%3Ca%20href%3D%22%23%22%20data-wf-bindings%3D%22%255B%257B%2522data-commerce-sku-id%2522%253A%257B%2522type%2522%253A%2522ItemRef%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.sku.id%2522%257D%257D%255D%22%20class%3D%22w-inline-block%22%20data-wf-cart-action%3D%22remove-item%22%20data-commerce-sku-id%3D%22%22%3E%3Cdiv%20class%3D%22cart-remove-link%22%3ERemove%3C%2Fdiv%3E%3C%2Fa%3E%3C%2Fdiv%3E%3C%2Fdiv%3E%3Cinput%20type%3D%22number%22%20data-wf-bindings%3D%22%255B%257B%2522value%2522%253A%257B%2522type%2522%253A%2522Number%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522numberPrecision%2522%252C%2522params%2522%253A%255B%25220%2522%252C%2522numberPrecision%2522%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.count%2522%257D%257D%252C%257B%2522data-commerce-sku-id%2522%253A%257B%2522type%2522%253A%2522ItemRef%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.sku.id%2522%257D%257D%255D%22%20data-wf-conditions%3D%22%257B%2522condition%2522%253A%257B%2522fields%2522%253A%257B%2522product%253Aec-product-type%2522%253A%257B%2522ne%2522%253A%2522e348fd487d0102946c9179d2a94bb613%2522%252C%2522type%2522%253A%2522Option%2522%257D%257D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D%2522%257D%22%20class%3D%22w-commerce-commercecartquantity%20input%20cart-quantity-input%22%20required%3D%22%22%20pattern%3D%22%5E%5B0-9%5D%2B%24%22%20inputMode%3D%22numeric%22%20name%3D%22quantity%22%20autoComplete%3D%22off%22%20data-wf-cart-action%3D%22update-item-quantity%22%20data-commerce-sku-id%3D%22%22%20value%3D%221%22%2F%3E%3C%2Fdiv%3E</script>
                                       <div class="w-commerce-commercecartlist cart-list" data-wf-collection="database.commerceOrder.userItems" data-wf-template-id="wf-template-e82a16f0-9b01-13bc-d231-3f58b93d68f4">
                                          <div class="w-commerce-commercecartitem cart-item-wrapper">
                                             <div class="cart-flex">
                                                <img data-wf-bindings="%5B%7B%22src%22%3A%7B%22type%22%3A%22ImageRef%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D.sku.f_main_image_4dr%22%7D%7D%5D" src="" alt="" class="w-commerce-commercecartitemimage cart-image-image border-radius-8px w-dyn-bind-empty"/>
                                                <div class="w-commerce-commercecartiteminfo mg-left-right-0-mbp">
                                                   <div data-wf-bindings="%5B%7B%22innerHTML%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D.product.f_name_%22%7D%7D%5D" class="w-commerce-commercecartproductname cart-item-title w-dyn-bind-empty"></div>
                                                   <div data-wf-bindings="%5B%7B%22innerHTML%22%3A%7B%22type%22%3A%22CommercePrice%22%2C%22filter%22%3A%7B%22type%22%3A%22price%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D.price%22%7D%7D%5D" class="cart-item-price">$ 0.00 USD</div>
                                                   <script type="text/x-wf-template" id="wf-template-e82a16f0-9b01-13bc-d231-3f58b93d68fb">%3Cli%20class%3D%22mg-bottom-4px%20pd-left-0%22%3E%3Cspan%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.product.f_sku_properties_3dr%255B%255D.name%2522%257D%257D%255D%22%20class%3D%22w-dyn-bind-empty%22%3E%3C%2Fspan%3E%3Cspan%3E%3A%20%3C%2Fspan%3E%3Cspan%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522CommercePropValues%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.userItems%255B%255D.product.f_sku_properties_3dr%255B%255D%2522%257D%257D%255D%22%20class%3D%22w-dyn-bind-empty%22%3E%3C%2Fspan%3E%3C%2Fli%3E</script>
                                                   <ul data-wf-bindings="%5B%7B%22optionSets%22%3A%7B%22type%22%3A%22CommercePropTable%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D.product.f_sku_properties_3dr[]%22%7D%7D%2C%7B%22optionValues%22%3A%7B%22type%22%3A%22CommercePropValues%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D.sku.f_sku_values_3dr%22%7D%7D%5D" class="w-commerce-commercecartoptionlist" data-wf-collection="database.commerceOrder.userItems%5B%5D.product.f_sku_properties_3dr" data-wf-template-id="wf-template-e82a16f0-9b01-13bc-d231-3f58b93d68fb">
                                                      <li class="mg-bottom-4px pd-left-0"><span data-wf-bindings="%5B%7B%22innerHTML%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D.product.f_sku_properties_3dr%5B%5D.name%22%7D%7D%5D" class="w-dyn-bind-empty"></span><span>: </span><span data-wf-bindings="%5B%7B%22innerHTML%22%3A%7B%22type%22%3A%22CommercePropValues%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D.product.f_sku_properties_3dr%5B%5D%22%7D%7D%5D" class="w-dyn-bind-empty"></span></li>
                                                   </ul>
                                                   <a href="#" data-wf-bindings="%5B%7B%22data-commerce-sku-id%22%3A%7B%22type%22%3A%22ItemRef%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D.sku.id%22%7D%7D%5D" class="w-inline-block" data-wf-cart-action="remove-item" data-commerce-sku-id="">
                                                      <div class="cart-remove-link">Remove</div>
                                                   </a>
                                                </div>
                                             </div>
                                             <input type="number" data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22Number%22%2C%22filter%22%3A%7B%22type%22%3A%22numberPrecision%22%2C%22params%22%3A%5B%220%22%2C%22numberPrecision%22%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D.count%22%7D%7D%2C%7B%22data-commerce-sku-id%22%3A%7B%22type%22%3A%22ItemRef%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D.sku.id%22%7D%7D%5D" data-wf-conditions="%7B%22condition%22%3A%7B%22fields%22%3A%7B%22product%3Aec-product-type%22%3A%7B%22ne%22%3A%22e348fd487d0102946c9179d2a94bb613%22%2C%22type%22%3A%22Option%22%7D%7D%7D%2C%22dataPath%22%3A%22database.commerceOrder.userItems%5B%5D%22%7D" class="w-commerce-commercecartquantity input cart-quantity-input" required="" pattern="^[0-9]+$" inputMode="numeric" name="quantity" autoComplete="off" data-wf-cart-action="update-item-quantity" data-commerce-sku-id="" value="1"/>
                                          </div>
                                       </div>
                                       <div class="w-commerce-commercecartfooter cart-footer">
                                          <div class="w-commerce-commercecartlineitem cart-line-item">
                                             <div class="cart-subtotal">Subtotal:</div>
                                             <div data-wf-bindings="%5B%7B%22innerHTML%22%3A%7B%22type%22%3A%22CommercePrice%22%2C%22filter%22%3A%7B%22type%22%3A%22price%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.subtotal%22%7D%7D%5D" class="w-commerce-commercecartordervalue cart-subtotal-number"></div>
                                          </div>
                                          <div>
                                             <div data-node-type="commerce-cart-quick-checkout-actions" style="display:none">
                                                <a role="button" tabindex="0" aria-haspopup="dialog" aria-label="Apple Pay" data-node-type="commerce-cart-apple-pay-button" style="background-image:-webkit-named-image(apple-pay-logo-white);background-size:100% 50%;background-position:50% 50%;background-repeat:no-repeat" class="w-commerce-commercecartapplepaybutton apple-pay-btn cart">
                                                   <div></div>
                                                </a>
                                                <a role="button" tabindex="0" aria-haspopup="dialog" data-node-type="commerce-cart-quick-checkout-button" style="display:none" class="w-commerce-commercecartquickcheckoutbutton">
                                                   <svg class="w-commerce-commercequickcheckoutgoogleicon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                                                      <defs>
                                                         <polygon id="google-mark-a" points="0 .329 3.494 .329 3.494 7.649 0 7.649"></polygon>
                                                         <polygon id="google-mark-c" points=".894 0 13.169 0 13.169 6.443 .894 6.443"></polygon>
                                                      </defs>
                                                      <g fill="none" fill-rule="evenodd">
                                                         <path fill="#4285F4" d="M10.5967,12.0469 L10.5967,14.0649 L13.1167,14.0649 C14.6047,12.6759 15.4577,10.6209 15.4577,8.1779 C15.4577,7.6339 15.4137,7.0889 15.3257,6.5559 L7.8887,6.5559 L7.8887,9.6329 L12.1507,9.6329 C11.9767,10.6119 11.4147,11.4899 10.5967,12.0469"></path>
                                                         <path fill="#34A853" d="M7.8887,16 C10.0137,16 11.8107,15.289 13.1147,14.067 C13.1147,14.066 13.1157,14.065 13.1167,14.064 L10.5967,12.047 C10.5877,12.053 10.5807,12.061 10.5727,12.067 C9.8607,12.556 8.9507,12.833 7.8887,12.833 C5.8577,12.833 4.1387,11.457 3.4937,9.605 L0.8747,9.605 L0.8747,11.648 C2.2197,14.319 4.9287,16 7.8887,16"></path>
                                                         <g transform="translate(0 4)">
                                                            <mask id="google-mark-b" fill="#fff">
                                                               <use xlink:href="#google-mark-a"></use>
                                                            </mask>
                                                            <path fill="#FBBC04" d="M3.4639,5.5337 C3.1369,4.5477 3.1359,3.4727 3.4609,2.4757 L3.4639,2.4777 C3.4679,2.4657 3.4749,2.4547 3.4789,2.4427 L3.4939,0.3287 L0.8939,0.3287 C0.8799,0.3577 0.8599,0.3827 0.8459,0.4117 C-0.2821,2.6667 -0.2821,5.3337 0.8459,7.5887 L0.8459,7.5997 C0.8549,7.6167 0.8659,7.6317 0.8749,7.6487 L3.4939,5.6057 C3.4849,5.5807 3.4729,5.5587 3.4639,5.5337" mask="url(#google-mark-b)"></path>
                                                         </g>
                                                         <mask id="google-mark-d" fill="#fff">
                                                            <use xlink:href="#google-mark-c"></use>
                                                         </mask>
                                                         <path fill="#EA4335" d="M0.894,4.3291 L3.478,6.4431 C4.113,4.5611 5.843,3.1671 7.889,3.1671 C9.018,3.1451 10.102,3.5781 10.912,4.3671 L13.169,2.0781 C11.733,0.7231 9.85,-0.0219 7.889,0.0001 C4.941,0.0001 2.245,1.6791 0.894,4.3291" mask="url(#google-mark-d)"></path>
                                                      </g>
                                                   </svg>
                                                   <svg class="w-commerce-commercequickcheckoutmicrosofticon" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                      <g fill="none" fill-rule="evenodd">
                                                         <polygon fill="#F05022" points="7 7 1 7 1 1 7 1"></polygon>
                                                         <polygon fill="#7DB902" points="15 7 9 7 9 1 15 1"></polygon>
                                                         <polygon fill="#00A4EE" points="7 15 1 15 1 9 7 9"></polygon>
                                                         <polygon fill="#FFB700" points="15 15 9 15 9 9 15 9"></polygon>
                                                      </g>
                                                   </svg>
                                                   <div>Pay with browser.</div>
                                                </a>
                                             </div>
                                             <a href="/checkout" value="Continue to Checkout" data-node-type="cart-checkout-button" class="w-commerce-commercecartcheckoutbutton btn-primary small" data-loading-text="Hang Tight...">Continue to Checkout</a>
                                          </div>
                                       </div>
                                    </form>
                                    <div class="w-commerce-commercecartemptystate flex-vertical pd-sides-24px">
                                       <div class="heading-h5-size mg-bottom-14px">No items found.</div>
                                       <a href="/credits" class="btn-primary w-button">Go to pricing</a>
                                    </div>
                                    <div style="display:none" data-node-type="commerce-cart-error" class="w-commerce-commercecarterrorstate error-state">
                                       <div class="w-cart-error-msg" data-w-cart-quantity-error="Product is not available in this quantity." data-w-cart-general-error="Something went wrong when adding this item to the cart." data-w-cart-checkout-error="Checkout is disabled on this site." data-w-cart-cart_order_min-error="The order minimum was not met. Add more items to your cart to continue." data-w-cart-subscription_error-error="Before you purchase, please use your email invite to verify your address so we can send order updates.">Product is not available in this quantity.</div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="header-right-side">
                     <a href="/post-a-product" class="btn-secondary small header-btn-hidden-on-tablet w-button">Post a free product</a><a href="/post-a-product" class="btn-primary small header-btn-hidden-on-tablet w-button"><span class="filled-icon-star"> </span>Post a featured product</a>
                     <div data-w-id="e82a16f0-9b01-13bc-d231-3f58b93d6920" class="hamburger-menu-wrapper w-nav-button">
                        <div class="hamburger-menu-bar top"></div>
                        <div class="hamburger-menu-bar bottom"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="section hero companies-hero border-bottom---bg-neutral-300 wf-section">
            <div class="container-default w-container">
               <div class="flex-horizontal text-center mg-bottom-48px">
                  <div data-w-id="dcce6311-7a00-5f1e-7656-ff45e93e0ce2" style="-webkit-transform:translate3d(0, 10%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 10%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 10%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 10%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="inner-container _424px">
                     <h1 class="display-1 mg-bottom-12px">Companies</h1>
                     <div class="inner-container _370px">
                        <p class="paragraph-small mg-bottom-0">Lorem ipsum dolor sit amet consectetur adipiscing elit etiam nisl tellus dolor egestas quis laoreet fames.</p>
                     </div>
                  </div>
               </div>
               <div class="w-dyn-list">
                  <div data-w-id="9af56367-ef55-ec5f-6400-5d4098eb9877" style="-webkit-transform:translate3d(0, 10%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 10%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 10%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 10%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" role="list" class="grid-3-columns w-dyn-items">
                    @foreach ($data['data'] as $surah)
                    <div role="listitem" class="w-dyn-item">
                        <a data-w-id="055d429e-843b-16c0-ae12-7b7827463208" href="{{ url('alquran/surat/'. $surah['nomor']) }}" class="card avatar-and-banner-card w-inline-block">
                           <div style="background-image:url(&quot;https://assets.website-files.com/63bed0273cfe5e611e742359/63c57221f6ef3b1a0c626ab3_facebook-banner-aggregator-x-webflow-template.png&quot;)" class="card-cms---banner"></div>
                           <div class="card-cms---description">
                              <div class="inner-container _284px">
                                 <div class="flex-vertical center text-center">
                                    <div class="mg-bottom-20px"><img src="https://quran.kemenag.go.id/apple-icon-144x144.png" alt="{{ $surah['nama'] }}" class="avatar-circle _03"/></div>
                                    <h2 class="heading-h4-size mg-bottom-6px"><td>{{ $surah['nama'] }}</td></h2>
                                    <div class="paragraph-small text-200 bold color-neutral-600 mg-bottom-12px"><td>Surah : {{ $surah['namaLatin'] }} ({{ $surah['arti'] }})</td></div>
                                    <div class="company-card">
                                       <div class="text"><td>Diturunkan di : {{ $surah['tempatTurun'] }}</td></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </a>
                     </div>
                     @endforeach
                     
                     
                     
                    {{-- End Item --}}
                  </div>
               </div>
            </div>
         </div>
         <footer class="footer-container">
            <div class="container-default w-container">
               <div class="footer-top-v1">
                  <div class="mg-bottom-80px">
                     <div class="w-layout-grid grid-4-columns footer-subgrid-main">
                        <div id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c67-91dd5701" data-w-id="a635aad1-350e-f68e-a786-d4dae50b1c67" class="inner-container _252px _100-tablet">
                           <a href="/home" class="footer-logo-wrapper w-inline-block"><img src="https://assets.website-files.com/63bed0273cfe5e3f80742329/63bf164efab9d2861750739a_black-logo-aggregator-x-webflow-template.svg" alt="Aggregator X Webflow Template - Logo"/></a>
                           <p class="mg-bottom-24px">Lorem ipsum dolor amet consectetur adipiscing elit duis blandit viverra.</p>
                           <div class="social-media-flex">
                              <a href="https://facebook.com/" class="social-icon-square w-inline-block">
                                 <div class="social-icon-font"></div>
                              </a>
                              <a href="https://twitter.com/" class="social-icon-square w-inline-block">
                                 <div class="social-icon-font"></div>
                              </a>
                              <a href="https://www.instagram.com/" class="social-icon-square w-inline-block">
                                 <div class="social-icon-font"></div>
                              </a>
                              <a href="https://www.linkedin.com/" class="social-icon-square w-inline-block">
                                 <div class="social-icon-font"></div>
                              </a>
                           </div>
                        </div>
                        <div id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c79-91dd5701" data-w-id="a635aad1-350e-f68e-a786-d4dae50b1c79">
                           <div class="text-300 footer-column-title">Main pages</div>
                           <div class="w-layout-grid grid-3-columns dropdown-subgrid footer-dropdown-subgrid">
                              <div id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c7d-91dd5701" class="w-layout-grid grid-1-column footer-subgrid"><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c7e-91dd5701" href="/home" class="footer-link-v1">Home</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c80-91dd5701" href="/" class="footer-link-v1">Home (Sales)</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c82-91dd5701" href="/about" class="footer-link-v1">About</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c84-91dd5701" href="/products" class="footer-link-v1">Products</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c86-91dd5701" href="https://aggregatortemplate.webflow.io/product-categories/business" class="footer-link-v1">Product category</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c88-91dd5701" href="https://aggregatortemplate.webflow.io/products/startuper" class="footer-link-v1">Product single</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c8a-91dd5701" href="https://aggregatortemplate.webflow.io/product-tags/communication" class="footer-link-v1">Product tag</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c8c-91dd5701" href="/creators" class="footer-link-v1">Creators</a></div>
                              <div id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c8e-91dd5701" class="w-layout-grid grid-1-column footer-subgrid"><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c8f-91dd5701" href="https://aggregatortemplate.webflow.io/creator/john-carter" class="footer-link-v1">Creators single</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c91-91dd5701" href="/moderators" class="footer-link-v1">Moderators</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c93-91dd5701" href="https://aggregatortemplate.webflow.io/moderators/lilly-woods" class="footer-link-v1">Moderator single</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c95-91dd5701" href="/companies" aria-current="page" class="footer-link-v1 w--current">Companies</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c97-91dd5701" href="https://aggregatortemplate.webflow.io/companies/webflow" class="footer-link-v1">Company single</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c99-91dd5701" href="/post-a-product" class="footer-link-v1">Post a free product</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c9b-91dd5701" href="/blog" class="footer-link-v1">Blog</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c9d-91dd5701" href="https://aggregatortemplate.webflow.io/blog-categories/marketing" class="footer-link-v1">Blog category</a></div>
                              <div id="w-node-a635aad1-350e-f68e-a786-d4dae50b1c9f-91dd5701" class="w-layout-grid grid-1-column footer-subgrid"><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ca0-91dd5701" href="https://aggregatortemplate.webflow.io/blog/10-product-design-tips-to-make-users-fall-in-love-with-your-app" class="footer-link-v1">Blog post</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ca2-91dd5701" href="/contact" class="footer-link-v1">Contact</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ca4-91dd5701" href="/credits" class="footer-link-v1">Credits</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ca6-91dd5701" href="https://aggregatortemplate.webflow.io/product/4-weeks-featured" class="footer-link-v1">Credit single</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ca8-91dd5701" href="/coming-soon" class="footer-link-v1">Coming soon</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1caa-91dd5701" href="https://brixtemplates.com/more-webflow-templates" target="_blank" class="more-webflow-templates-link">More Webflow Templates</a></div>
                           </div>
                        </div>
                        <div id="w-node-a635aad1-350e-f68e-a786-d4dae50b1cac-91dd5701" data-w-id="a635aad1-350e-f68e-a786-d4dae50b1cac">
                           <div class="text-300 footer-column-title">Categories</div>
                           <div class="categories-card-wrapper v1">
                              <div class="w-dyn-list">
                                 <div role="list" class="grid-1-column gap-row-24px w-dyn-items">
                                    <div role="listitem" class="w-dyn-item">
                                       <a href="/product-categories/business" class="category-footer-link w-inline-block">
                                          <div class="category---description-wrapper">
                                             <div><img src="https://assets.website-files.com/63bed0273cfe5e611e742359/63c57f0a578243231624ebd4_briefcase-aggregator-x-webflow-template.svg" alt="Business" class="_w-h-20px"/></div>
                                             <div class="category---description">
                                                <div class="text-100 bold">Business</div>
                                                <div class="category---short-description">Massa sit praesent leo morbi</div>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                    <div role="listitem" class="w-dyn-item">
                                       <a href="/product-categories/design" class="category-footer-link w-inline-block">
                                          <div class="category---description-wrapper">
                                             <div><img src="https://assets.website-files.com/63bed0273cfe5e611e742359/63c57eeec8b8535877b91cd0_pencil-aggregator-x-webflow-template.svg" alt="Design" class="_w-h-20px"/></div>
                                             <div class="category---description">
                                                <div class="text-100 bold">Design</div>
                                                <div class="category---short-description">Donec aliquet urna eu</div>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                    <div role="listitem" class="w-dyn-item">
                                       <a href="/product-categories/development" class="category-footer-link w-inline-block">
                                          <div class="category---description-wrapper">
                                             <div><img src="https://assets.website-files.com/63bed0273cfe5e611e742359/63c57ec7f0ee7312b9f4e65a_monitor-aggregator-x-webflow-template.svg" alt="Development" class="_w-h-20px"/></div>
                                             <div class="category---description">
                                                <div class="text-100 bold">Development</div>
                                                <div class="category---short-description">Suspendisse et non et dolor</div>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                    <div role="listitem" class="w-dyn-item">
                                       <a href="/product-categories/marketing" class="category-footer-link w-inline-block">
                                          <div class="category---description-wrapper">
                                             <div><img src="https://assets.website-files.com/63bed0273cfe5e611e742359/63c57e995c1979ddb6479977_pencil-simple-aggregator-x-webflow-template.svg" alt="Marketing" class="_w-h-20px"/></div>
                                             <div class="category---description">
                                                <div class="text-100 bold">Marketing</div>
                                                <div class="category---short-description">Id morbi eget velit</div>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ce1-91dd5701" data-w-id="a635aad1-350e-f68e-a786-d4dae50b1ce1">
                           <div class="text-300 footer-column-title">Utility Pages</div>
                           <div class="w-layout-grid grid-1-column footer-subgrid"><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ce5-91dd5701" href="/template-pages/start-here" class="footer-link-v1">Start here</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ce7-91dd5701" href="/template-pages/style-guide" class="footer-link-v1">Styleguide</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ce9-91dd5701" href="https://aggregatortemplate.webflow.io/404" class="footer-link-v1">404 Not found</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ceb-91dd5701" href="https://aggregatortemplate.webflow.io/401" class="footer-link-v1">Password protected</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1ced-91dd5701" href="/template-pages/licenses" class="footer-link-v1">Licenses</a><a id="w-node-a635aad1-350e-f68e-a786-d4dae50b1cef-91dd5701" href="/template-pages/changelog" class="footer-link-v1">Changelog</a></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div data-w-id="a635aad1-350e-f68e-a786-d4dae50b1cf1" class="w-layout-grid grid-2-columns">
                  <div id="w-node-a635aad1-350e-f68e-a786-d4dae50b1cf2-91dd5701" class="footer-card">
                     <div class="footer-card-wrapper">
                        <div class="flex-horizontal start flex-vertical---mbp">
                           <div class="circle _48px blue">
                              <div class="line-rounded-icon arrow-up"></div>
                           </div>
                           <div class="mg-left-12px mg-left-0-mbp">
                              <div class="heading-h5-size">Publish a free product!</div>
                              <div class="paragraph-small">Lorem ipsum dolor amet consectetur.</div>
                           </div>
                        </div>
                        <div class="width-100-mbp"><a data-w-id="a635aad1-350e-f68e-a786-d4dae50b1cfe" href="/post-a-product" class="btn-primary small w-button">Publish now<span class="line-rounded-icon link-icon-right mg-left-4px"></span></a></div>
                     </div>
                     <div class="floating-item circle-floating"></div>
                  </div>
                  <div id="w-node-a635aad1-350e-f68e-a786-d4dae50b1d03-91dd5701" class="footer-card blue">
                     <div class="footer-card-wrapper">
                        <div class="flex-horizontal start flex-vertical---mbp">
                           <div class="circle _48px white"><img src="https://assets.website-files.com/63bed0273cfe5e3f80742329/63bf03e3f922f863ce29b6fb_star-aggregator-x-webflow-template.svg" alt=""/></div>
                           <div class="mg-left-12px mg-left-0-mbp">
                              <div class="heading-h5-size color-neutral-100">Publish a featured product!</div>
                              <div class="paragraph-small color-neutral-300">Lorem ipsum dolor amet consectetur.</div>
                           </div>
                        </div>
                        <div class="width-100-mbp"><a data-w-id="a635aad1-350e-f68e-a786-d4dae50b1d0e" href="/post-a-product" class="btn-primary small black w-button">Publish now<span class="line-rounded-icon link-icon-right mg-left-4px"></span></a></div>
                     </div>
                     <div class="floating-item circle-floating white"></div>
                  </div>
               </div>
               <div class="footer-bottom-v1">
                  <div data-w-id="a635aad1-350e-f68e-a786-d4dae50b1d14" class="footer-bottom-flex">
                     <p class="mg-bottom-0 text-center">Copyright © Aggregator X | Designed by <a href="https://brixtemplates.com/" target="_blank" class="footer-credits">BRIX Templates</a> - Powered by <a href="https://webflow.com/" target="_blank" class="footer-credits">Webflow</a></p>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <div data-w-id="8a8a9c95-b6c6-0434-ac96-7cbb322c17e6" class="loading-bar-wrapper">
         <div class="loading-bar"></div>
      </div>
      <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=63bed0273cfe5e3f80742329" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script><script src="https://assets.website-files.com/63bed0273cfe5e3f80742329/js/webflow.ba9cd15f5.js" type="text/javascript"></script><!--[if lte IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
   </body>
</html>