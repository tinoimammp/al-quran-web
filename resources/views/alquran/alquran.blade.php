<!DOCTYPE html>
<html>
<head>
    <title>Data</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Detail</th>
            </tr>
        </thead>
        <tbody>
           @foreach ($data['data'] as $surah)
        <tr>
            <td>{{ $surah['nomor'] }}</td>
            <td>{{ $surah['nama'] }}</td>
            <td>{{ $surah['namaLatin'] }}</td>
            <td><a href="{{ url('alquran/surat/' . $surah['nomor']) }}" class="btn btn-pill btn-success btn-xs">Lihat</a></td>
        </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>
