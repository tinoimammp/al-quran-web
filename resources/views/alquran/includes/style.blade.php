<link rel="stylesheet" type="text/css" href="{{ asset('../css/font-awesome.css') }}">
<!-- ico-font-->
<link rel="stylesheet" type="text/css" href="{{ asset('../css/icofont.css') }}">
<!-- Themify icon-->
<link rel="stylesheet" type="text/css" href="{{ asset('../css/themify.css') }}">
<!-- Flag icon-->
<link rel="stylesheet" type="text/css" href="{{ asset('../css/flag-icon.css') }}">
<!-- Feather icon-->
<link rel="stylesheet" type="text/css" href="{{ asset('../css/feather-icon.css') }}">
<!-- Plugins css start-->
<link rel="stylesheet" type="text/css" href="{{ asset('../css/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../css/slick-theme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../css/scrollbar.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../css/animate.css') }}">
<!-- Plugins css Ends-->
<!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap"
    rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap"
    rel="stylesheet">
<!-- Bootstrap css-->
<link rel="stylesheet" type="text/css" href="{{ asset('../css/bootstrap.css') }}">
<!-- App css-->
<link rel="stylesheet" type="text/css" href="{{ asset('../css/style.css') }}">
<link id="color" rel="stylesheet" href="{{ asset('../css/color-1.css" media="screen') }}">
<!-- Responsive css-->
<link rel="stylesheet" type="text/css" href="{{ asset('../css/responsive.css') }}">