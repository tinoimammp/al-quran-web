   <!-- Page Sidebar Start-->
      <div class="sidebar-wrapper" sidebar-layout="stroke-svg">
          <div>
          <div class="logo-wrapper"><a href="{{ url('dashboard') }}"><img class="img-fluid for-light" src="{{ asset('../images/kubika.png')}}" alt><img
                  class="img-fluid for-dark" src="{{ asset('../images/kubika.png')}}" alt></a>
              <div class="back-btn"><i class="fa fa-angle-left"></i></div>
              <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
          </div>
          <div class="logo-icon-wrapper"><a href="{{ url('dashboard') }}"><img class="img-fluid" src="{{ asset('../images/kubikal-icon.png')}}" alt></a>
          </div>
          <nav class="sidebar-main">
              <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
                <div id="sidebar-menu">
                  <ul class="sidebar-links" id="simple-bar">
                    <li class="back-btn"><a href="{{ url('dashboard') }}"><img class="img-fluid" src="{{ url('../images/kubikal-icon.png') }}" alt></a>
                      <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2"
                          aria-hidden="true"></i></div>
                    </li>
                    <li class="pin-title sidebar-main-title">
                      <div>
                        <h6>Pinned</h6>
                      </div>
                    </li>
                    <li class="sidebar-main-title">
                      <div>
                        <h6 class="lan-l">Menu Utama</h6>
                      </div>
                    </li>
                    <li class="sidebar-list"><i class="fa fa-thumb-tack"></i>
                      <a class="sidebar-link sidebar-title link-nav" href="{{ url('dashboard') }}">
                        <i data-feather="home" class="stroke-icon"></i>
                        <i data-feather="home" class="fill-icon"></i> 
                          <span>Dashboard</span>
                      </a>
                    </li>
                    <li class="sidebar-list"><i class="fa fa-thumb-tack"></i>
                      <a class="sidebar-link sidebar-title" href="#">
                        <i data-feather="mail" class="stroke-icon"></i>
                        <i data-feather="mail" class="fill-icon"></i>
                        <span class="">Data Master</span>
                      </a>
                      <ul class="sidebar-submenu">
                      <li><a class="" href="{{ url('e-arsip/datamaster/rak') }}">Data Rak</a></li>
                      <li><a class="" href="{{ url('e-arsip/datamaster/ruang') }}">Data Ruang</a></li>
                      <li><a class="" href="{{ url('e-arsip/datamaster/kabinet') }}">Data Kabinet</a></li>
                      <li><a class="" href="{{ url('e-arsip/datamaster/brankas') }}">Data Brankas</a></li>
                      <li><a class="" href="{{ url('e-arsip/datamaster/ordner') }}">Data Ordner</a></li>                      </ul>
                    </li>
                    <li class="sidebar-list"><i class="fa fa-thumb-tack"></i><a class="sidebar-link sidebar-title" href="#">
                        <i data-feather="message-square" class="stroke-icon"></i><i data-feather="message-square"
                          class="fill-icon"></i><span class="">Data Transaksi</span></a>
                      <ul class="sidebar-submenu">
                        <li><a href="{{ url('e-arsip/peminjaman') }}">Approval Peminjaman</a></li>
                        <li><a href="{{ url('e-arsip/pengembalian') }}">Approval Pengembalian</a></li>
                        <li><a href="{{ url('e-arsip/histori-peminjaman') }}">Histori Peminjaman</a></li>
                      </ul>
                    </li>
                
                    <li class="sidebar-list"><i class="fa fa-thumb-tack"></i>
                      <a class="sidebar-link sidebar-title link-nav" href="{{ url('notulen') }}">
                        <i data-feather="edit-3" class="stroke-icon"></i><i data-feather="edit-3"
                          class="fill-icon"></i><span>Data Dokumen</span></a>
                    </li>
                  
                  
                    <li class="sidebar-main-title">
                      <div>
                        <h6 class="">Menu Lainnya</h6>
                      </div>
                    </li>
                    <li class="sidebar-list"><i class="fa fa-thumb-tack"></i>
                      <a class="sidebar-link sidebar-title link-nav" href="disposisi.html">
                        <i data-feather="log-out" class="stroke-icon"></i><i data-feather="log-out"
                          class="fill-icon"></i><span>Keluar</span></a>
                    </li>
                  </ul>
                </div>
              <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
          </nav>
          </div>
      </div>