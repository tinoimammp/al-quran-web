<!-- latest jquery-->
<script src="{{ asset('../js/jquery.min.js') }}"></script>
<!-- Bootstrap js-->
<script src="{{ asset('../js/bootstrap.bundle.min.js') }}"></script>
<!-- feather icon js-->
<script src="{{ asset('../js/feather.min.js') }}"></script>
<script src="{{ asset('../js/feather-icon.js') }}"></script>
<!-- scrollbar js-->
<script src="{{ asset('../js/simplebar.js') }}"></script>
<script src="{{ asset('../js/custom.js') }}"></script>
<!-- Sidebar jquery-->
<script src="{{ asset('../js/config.js') }}"></script>
<!-- Plugins JS start-->
<script src="{{ asset('../js/sidebar-menu.js') }}"></script>
<script src="{{ asset('../js/clock.js') }}"></script>
<script src="{{ asset('../js/slick.min.js') }}"></script>
<script src="{{ asset('../js/slick.js') }}"></script>
<script src="{{ asset('../js/header-slick.js') }}"></script>
<script src="{{ asset('../js/apex-chart.js') }}"></script>
<script src="{{ asset('../js/stock-prices.js') }}"></script>
<script src="{{ asset('../js/moment.min.js') }}"></script>
<script src="{{ asset('../js/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('../js/default.js') }}"></script>
<script src="{{ asset('../js/index.js') }}"></script>
<script src="{{ asset('../js/handlebars_1.js') }}"></script>
<script src="{{ asset('../js/typeahead.bundle.js') }}"></script>
<script src="{{ asset('../js/typeahead.custom.js') }}"></script>
<script src="{{ asset('../js/handlebars.js') }}"></script>
<script src="{{ asset('../js/typeahead-custom.js') }}"></script>
<script src="{{ asset('../js/height-equal.js') }}"></script>
<script src="{{ asset('../js/wow.min.js') }}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{ asset('../js/script.js') }}"></script>
<script src="{{ asset('../js/customizer.js') }}"></script>
<!-- Plugin used-->
<script>new WOW().init();</script>